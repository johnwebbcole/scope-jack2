function arm() {
  var arm = Parts.Cylinder(util.inch(1), util.inch(1 / 8)).stretch(
    "x",
    util.inch(6)
  );
  var post = Parts.Cylinder(util.inch(3 / 8), util.inch(1 / 8));
  var hole = Parts.Cylinder(util.inch(1 / 4) + 1, util.inch(1 / 8));
  return arm
    .subtract(
      arm
        .enlarge(-4, -4, -util.inch(1 / 16))
        .snap(arm, "z", "inside+")
        .align(arm, "xy")
    )
    .union([
      post,
      post.translate([util.inch(0.5), 0, 0]),
      post.translate([util.inch(1), 0, 0]),
      post.translate([util.inch(1.5), 0, 0]),
      post.translate([util.inch(2), 0, 0]),
      post.translate([util.inch(2.5), 0, 0]),
      post.translate([util.inch(3), 0, 0]),
      post.translate([util.inch(3.5), 0, 0]),
      post.translate([util.inch(4), 0, 0]),
      post.translate([util.inch(4.5), 0, 0]),
      post.translate([util.inch(5), 0, 0]),
      post.translate([util.inch(5.5), 0, 0]),
      post.translate([util.inch(6), 0, 0])
    ])
    .subtract([
      hole,
      hole.translate([util.inch(0.5), 0, 0]),
      hole.translate([util.inch(1), 0, 0]),
      hole.translate([util.inch(1.5), 0, 0]),
      hole.translate([util.inch(2), 0, 0]),
      hole.translate([util.inch(2.5), 0, 0]),
      hole.translate([util.inch(3), 0, 0]),
      hole.translate([util.inch(3.5), 0, 0]),
      hole.translate([util.inch(4), 0, 0]),
      hole.translate([util.inch(4.5), 0, 0]),
      hole.translate([util.inch(5), 0, 0]),
      hole.translate([util.inch(5.5), 0, 0]),
      hole.translate([util.inch(6), 0, 0])
    ])
    .Center()
    .color("gray");
}
