function MotorMount(screw, rail, block) {
  var bracket = util.group("motorMount");
  var lockNut = thrustAssembly(screw)
    .rotate("collar", "y", 180)
    .snap("thrustBearingbottom", screw, "x", "inside-", util.inch(1));
  bracket.add(lockNut);

  var motor = Motor2()
    .snap("pto", screw, "x", "outside+")
    .rotate("body", "y", -90)
    .rotate("shaft", "x", 90)
    .align("shaft", screw, "yz")
    .snap("dshaft", screw, "x", "outside+");

  bracket.add(motor, "motor");

  /**
   * MotorBlock
   */
  var motorBlockThickness = motor.parts.pto.size().x;

  var motorBlock = Parts.Cube([
    motorBlockThickness,
    util.inch(2),
    util.inch(1.125)
  ])
    .align(motor.parts.body, "y")
    .snap(motor.parts.body, "x", "outside-")
    .snap(rail, "z", "inside-");

  var motorBlockTop = Parts.Cylinder(util.inch(2), motorBlockThickness)
    .rotateY(90)
    .bisect("z")
    .parts.positive.align(motor.parts.body, "y")
    .snap(motor.parts.body, "x", "outside-")
    .snap(motorBlock, "z", "outside-");

  var motorBlockBase = Parts.Cube([
    util.inch(2.25),
    util.inch(2.5),
    util.inch(0.25)
  ])
    .snap(motorBlock, "z", "inside-")
    .snap(motorBlock, "x", "inside+", util.inch(0.625))
    .snap(block, "y", "inside+");

  var motorBlockWing = Parts.Triangle(util.inch(1.125), 5)
    .rotateZ(45)
    .rotateX(90)
    .snap(motorBlockBase, "z", "outside-")
    .snap(motorBlock, "x", "outside+", 0.2)
    .snap(motorBlock, "y", "inside+");

  var motorScrew1 = Hardware.Screw.PanHead(
    ImperialWoodScrews["#10"],
    util.inch(0.75),
    "close",
    { name: "motorScrew1" }
  )
    .rotate("head", "y", 180)
    .snap("head", motorBlockBase, "z", "outside-")
    .snap("head", motorBlockBase, "y", "inside-", util.inch(0.125))
    .snap("head", motorBlockBase, "x", "inside+", -util.inch(0.125));
  bracket.add(motorScrew1, "motorScrew1", false, "motorScrew1", "head,thread");

  var motorScrew2 = motorScrew1
    .clone("motorScrew2")
    .snap("head", motorBlockBase, "x", "inside-", util.inch(0.125));
  bracket.add(motorScrew2, "motorScrew2", false, "motorScrew2", "head,thread");

  var motorScrew3 = motorScrew2
    .clone("motorScrew3")
    .snap("head", motorBlockBase, "y", "inside+", -util.inch(0.125));
  bracket.add(motorScrew3, "motorScrew3", false, "motorScrew3", "head,thread");

  var motorScrew4 = motorScrew1
    .clone("motorScrew4")
    .snap("head", motorBlockBase, "y", "inside+", -util.inch(0.125));
  bracket.add(motorScrew4, "motorScrew4", false, "motorScrew4", "head,thread");

  bracket.add(
    motorBlock
      .union([
        motorBlockBase,
        motorBlockTop,
        motorBlockWing,
        motorBlockWing.snap(motorBlock, "y", "inside-")
      ])
      .color("green", 0.75)
      .subtract([
        motorScrew1.parts.tap,
        motorScrew2.parts.tap,
        motorScrew3.parts.tap.stretch("y", 10).color("red"),
        motorScrew4.parts.tap.stretch("y", 10).color("red"),
        motor.parts.screw1tap,
        motor.parts.screw2tap,
        motor.parts.screw3tap,
        motor.parts.screw4tap,
        motor.parts.pto.enlarge(1, 1, 1)
      ]),
    "motorBlock"
  );

  return bracket;
}
