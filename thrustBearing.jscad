function ThrustBearing() {
  var top = Parts.Cylinder(20.22, 0.8)
    .subtract(Parts.Cylinder(9.65, 0.8))
    .color("blue");
  var middle = Parts.Cylinder(20.18, 2)
    .subtract(Parts.Cylinder(9.7, 2))
    .snap(top, "z", "outside+")
    .color("gray");
  var bottom = Parts.Cylinder(20.22, 0.8)
    .subtract(Parts.Cylinder(9.65, 0.8))
    .snap(middle, "z", "outside+")
    .color("red");

  var bearing = util.group({ top, middle, bottom });

  /**
   * Add a hidden shell for use when subtracting a place for the bearing
   */
  bearing.add(
    Parts.Cylinder(21, 4)
      .color("darkred")
      .snap(top, "z", "inside+"),
    "shell",
    true
  );

  return bearing;
}

function ThrustBearing_1_2() {
  var thrustBearingHeight = 8.88;
  var thickness = thrustBearingHeight / 3;
  var top = Parts.Cylinder(28, thickness)
    .subtract(Parts.Cylinder(16, thickness))
    .color("blue");
  var middle = Parts.Cylinder(27.2, thickness)
    .subtract(Parts.Cylinder(16, thickness))
    .snap(top, "z", "outside+")
    .color("gray");
  var bottom = Parts.Cylinder(28, thickness)
    .subtract(Parts.Cylinder(15, thickness))
    .snap(middle, "z", "outside+")
    .color("red");

  var bearing = util.group({ top, middle, bottom }).setName("thrustBearing");

  /**
   * Add a hidden shell for use when subtracting a place for the bearing
   */
  bearing.add(
    Parts.Cylinder(29, thrustBearingHeight)
      .color("green")
      .snap(top, "z", "inside+"),
    "shell",
    true
  );

  return bearing;
}

function ThrustBearing_1_2_pin_rollers() {
  var thrustBearingHeight = 3.55;
  var thickness = thrustBearingHeight / 3;
  var top = Parts.Cylinder(23.38, thickness)
    .subtract(Parts.Cylinder(12.78, thickness))
    .color("blue");
  var middle = Parts.Cylinder(23.45, thickness)
    .subtract(Parts.Cylinder(12.9, thickness))
    .snap(top, "z", "outside+")
    .color("gray");
  var bottom = Parts.Cylinder(23.38, thickness)
    .subtract(Parts.Cylinder(12.78, thickness))
    .snap(middle, "z", "outside+")
    .color("red");

  var bearing = util.group({ top, middle, bottom }).setName("thrustBearing");

  /**
   * Add a hidden shell for use when subtracting a place for the bearing
   */
  bearing.add(
    Parts.Cylinder(24, thrustBearingHeight)
      .color("green")
      .snap(top, "z", "inside+"),
    "shell",
    true
  );

  return bearing;
}
