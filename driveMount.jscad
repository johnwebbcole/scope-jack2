function DriveMount(screw, rail) {
  var bracket = util.group("driveMount");
  var thrustAssm = thrustAssembly(screw)
    .rotate("collar", "y", 180)
    .snap("bearing", screw, "x", "inside-", util.inch(2));
  bracket.add(thrustAssm, undefined, true);

  console.log("DriveMount", bracket);

  var block = Parts.Cube([util.inch(0.875), util.inch(1.5), util.inch(1.75)])
    .align(screw, "y")
    .snap(bracket.parts.bearing, "x", "inside+")

    .snap(rail, "z", "inside-")
    .color("green", 0.125);

  // BetterTriangle is located in pivotMount.jscad
  var braceInside = BetterTriangle(
    util.inch(1.0),
    util.inch(1.75),
    util.inch(1.5)
  )
    .rotateX(-90)
    // .rotateY(-45)
    .align(block, "yz")
    .snap(block, "x", "outside+")
    .color("green", 0.25);

  var braceOutside = braceInside.mirroredX().snap(block, "x", "outside-");

  var washerA = Hardware.Washer(ImperialWashers["1/4"], "loose", {
    name: "washerA"
  })
    .snap("washer", block, "z", "outside-")
    .snap("washer", block, "y", "inside+", 1)
    .snap("washer", block, "x", "inside-"); // outside edge of block
  // .align("washer", block, "x"); // center of block
  bracket.add(washerA, "washerA", true, "washerA", "washer");

  var washerB = washerA.clone().snap("washer", block, "y", "inside-", -1);
  bracket.add(washerB, "washerB", true, "washerB", "washer");
  var screw1 = Hardware.Bolt(util.inch(2.5), ImperialBolts["1/4 hex"], "loose")
    .rotate("head", "y", 180)
    .snap("head", washerA.parts.washer, "z", "outside-")
    .align("head", washerA.parts.washer, "xy");
  bracket.add(screw1, "screw1", true, "screw1", "head,thread");

  var screw2 = screw1.clone().align("head", washerB.parts.washer, "xy");
  bracket.add(screw2, "screw2", true, "screw2", "head,thread");

  /**
   * outboard bolts
   */
  var washerC = Hardware.Washer(ImperialWashers["1/4"], "loose")
    .snap("washer", block, "z", "inside-")
    .snap("clearance", block, "y", "inside+", 1)
    .snap("washer", braceOutside, "x", "inside+")
    .translate([0, 0, util.inch(0.375)]);
  bracket.add(washerC, "washerC", true, "washerC", "washer");

  var screwC = Hardware.Bolt(util.inch(1.25), ImperialBolts["1/4 hex"], "loose")
    .rotate("head", "y", 180)
    .snap("head", washerC.parts.washer, "z", "outside-")
    .align("head", washerC.parts.washer, "xy");
  // .translate([0, -1, 0]);
  bracket.add(screwC, "screwC", true, "screwC", "head,thread");

  var washerD = washerC
    .clone()
    .snap("washer", block, "x", "outside+", -util.inch(0.25))
    .snap("clearance", block, "y", "inside-", -1);
  // .translate([0, 0, -util.inch(0.75)]);
  bracket.add(washerD, "washerD", true, "washerD", "washer");

  var screwD = screwC
    .clone()
    .align("head", washerD.parts.washer, "xy")
    .snap("head", washerD.parts.washer, "z", "outside-");
  // .translate([0, 1, 0]);
  bracket.add(screwD, "screwD", true, "screwD", "head,thread");

  /**
   * Motor Connector 16x22
   */
  var connector = Parts.Cylinder(20, 22)
    .rotateY(90)
    .align(screw, "yz")
    .snap(screw1.parts.thread, "x", "outside+", util.inch(-0.125))
    .color("silver");
  var connectorSetScrewAccessHole = Parts.Cylinder(7, 20)
    .align(connector, "y")
    .snap(connector, "x", "inside+", -3)
    .snap(screw, "z", "outside-")
    .color("darkred");

  var base = block.union([braceInside, braceOutside]);
  var baseBBox = Parts.BBox(base);
  bracket.add(
    base
      .union([
        washerC.parts.clearance
          .stretch("z", 11.1)
          .snap(washerC.parts.washer, "z", "outside+")
          .color("green")
          .intersect(baseBBox),
        washerD.parts.clearance
          .stretch("z", 11.1)
          .snap(washerD.parts.washer, "z", "outside+")
          .color("green")
          .intersect(baseBBox)
      ])
      .subtract([
        // bracket.parts.thrustBearingbottom.stretch(
        //   "x",
        //   bracket.combine("collar,bearing").size().x
        // ),
        bracket.parts.bearing
          .enlarge([
            0,
            Hardware.Clearances.loose * 1.5,
            Hardware.Clearances.loose * 1.5
          ])
          .stretch("x", bracket.combine("collar,bearing").size().x)
          .snap(bracket.parts.bearing, "x", "inside-")
          .color("yellow"),
        bracket.parts.collarshell.stretch("x", 5),
        // bracket.parts.washer1clearance
        //   .stretch("x", bracket.combine("nut1").size().x * 2)
        //   .snap(bracket.combine("nut1"), "x", "inside-")
        //   .color("yellow"),
        screw.enlarge(1, 1, 1),
        screw1.parts.tap,
        screw2.parts.tap,
        washerC.parts.clearance.stretch("z", util.inch(0.75)).color("darkred"),
        screwC.parts.tap,
        washerD.parts.clearance.stretch("z", 50).color("darkred"),
        screwD.parts.tap,
        connector,
        connectorSetScrewAccessHole
      ]),
    // .union([connector, connectorSetScrewAccessHole]),
    "block"
  );

  return bracket;
}
