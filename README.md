# scope-jack2

> A sissor style jack for a telescope buggy.

<vuepress-open-jscad design="scope-jack2.jscad" :panel="{size:223}" :camera="{position: {x: 0, y: 0, z: 223},clip: {min: 1, max: 1000}}"></vuepress-open-jscad>

## Running

The jscad project `scope-jack2` uses gulp to create a `dist/scope-jack2.jscad` file and watches your source for changes. You can drag the `dist/scope-jack2.jscad` directory into the drop area on [openjscad.org](http://openjscad.org). Make sure you check `Auto Reload` and any time you save, gulp creates the `dist/scope-jack2.jscad` file, and your model should refresh.

### start

`npm start` or `npm run start` will launch gulp, and create `dist/scope-jack2.jscad` . It also watches for file changes and recreates the dist file.

### clean

Deletes the `dist` directory when you run `npm run clean`.

### inject

Run gulp to combine the source files and inject the dependent libraries with `npm run inject`. Libraries are found using a gulp plugin that looks for a `jscad.json` file in a package. These files are combined and minimized before injecting into the dist file.

### build

Build the [vuepress] static site by running `npm run build`. This script combines the readme with a [vue-openjscad] component to display a live view of the model. The [baseUrl](https://vuepress.vuejs.org/guide/assets.html#base-url) is set with the `BASEPATH` environment variable. It defaults to `/scope-jack2/`. When hosted on [GitLab], the `.gitlab-ci.yml` CICD file uses this script to publish to [GitLab Pages].

See the [vue-openjscad] package for information on modifying options like the grid or initial camera position.

### serve

Run [vuepress] in dev mode with `npm run serve`. This script watches for file changes and hot reloads changes made to the README file. Changes to the model are not automatically reloaded; a manual reload is required.

## jscad-utils

The example project uses [jscad-utils]. These utilities are a set of utilities that make object creation and alignment easier. To remove it, `npm uninstall --save jscad-utils`, and remove the
`util.init(CSG);` line in `scope-jack2.jscad`.

## Other libraries

You can search [NPM](https://www.npmjs.com/search?q=jscad) for other jscad libraries. Installing them with NPM and running `gulp` should create a `dist/scope-jack2.jscad` will all dependencies injected into the file.

For example, to load a [RaspberryPi jscad library] and show a Raspberry Pi Model B, install jscad-raspberrypi using `npm install --save jscad-raspberrypi`. Then return a combined `BPlus` group from the `main()` function.

```javascript
main() {
  util.init(CSG);

  return RaspberryPi.BPlus().combine();
}

// ********************************************************
// Other jscad libraries are injected here.  Do not remove.
// Install jscad libraries using NPM
// ********************************************************
// include:js
// endinject
```

## License

ISC © [John Cole](https://jwc.dev)

[raspberrypi jscad library]: https://gitlab.com/johnwebbcole/jscad-raspberrypi
[vuepress]: https://vuepress.vuejs.org/
[jscad-utils]: https://www.npmjs.com/package/jscad-utils
[gitlab]: https://gitlab.com/
[gitlab pages]: https://gitlab.com/help/user/project/pages/index.md
[vue-openjscad]: https://gitlab.com/johnwebbcole/vue-openjscad
