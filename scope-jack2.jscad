// title      : scopeJack2
// author     : John Cole
// license    : ISC License
// file       : scopeJack2.jscad

/* exported main, getParameterDefinitions */

function getParameterDefinitions() {
  var ENABLED = {
    screw: true,
    rail: true,
    hardware: true,
    pivotMount: true,
    driveMount: true,
    motorMount: true,
    driveBushing: false,
    carriage: true,
    carriageA: false,
    carriageB: false,
    roller: false,
    bushing: false,
    motorAdapter: false,
    tensionTool: false
  };
  return [
    { type: "group", name: "Parts" },
    ..."screw,rail,hardware,pivotMount,driveMount,motorMount,driveBushing,carriage,carriageA,carriageB,roller,arms,bushing,motorAdapter,tensionTool"
      .split(",")
      .map(name => {
        return {
          name,
          type: "checkbox",
          checked: ENABLED[name]
        };
      }),
    {
      name: "resolution",
      type: "choice",
      values: [0, 1, 2, 3, 4],
      captions: [
        "low (8,24)",
        "normal (12,32)",
        "high (24,64)",
        "very high (48,128)",
        "ultra high (96,256)"
      ],
      default: 0,
      initial: 0,
      caption: "Resolution:"
    },
    { type: "group", name: "View" },
    {
      name: "center",
      type: "checkbox",
      checked: true
    },
    { type: "group", name: "Cutaway" },
    {
      name: "cutawayEnable",
      caption: "Enable:",
      type: "checkbox",
      checked: false
    },
    {
      name: "cutawayAxis",
      type: "choice",
      values: ["x", "y", "z"],
      initial: "y",
      caption: "Axis:"
    }
  ];
}

function main(params) {
  var start = performance.now();
  var resolutions = [
    [8, 24],
    [12, 32],
    [24, 64],
    [48, 128],
    [96, 256]
  ];
  var [defaultResolution3D, defaultResolution2D] = resolutions[
    parseInt(params.resolution)
  ];
  CSG.defaultResolution3D = defaultResolution3D;
  CSG.defaultResolution2D = defaultResolution2D;
  util.init(CSG, { debug: "" });

  /**
   * Add bounding boxes to cache.  Some parts require these to
   * position themselves.
   */
  _cache.screw_bounds = cube({
    size: [254, 12.7, 12.700000000000015]
  }).translate([-127, -6.35, -6.350000000000008]);

  _cache.carriage_bounds = cube({
    size: [25.40000000000002, 50.800000000000026, 50.79999999999998]
  }).translate([-12.700000000000003, -25.400000000000013, -25.4]);

  _cache.pivotMount_bounds = cube({
    size: [73.02499999999999, 38.10000000000002, 44.45000000000001]
  }).translate([61.92999999999999, -19.05000000000001, -29.368750000000013]);

  _cache.rail_bounds = cube({
    size: [406.4, 12.7, 1.5874999999999986]
  }).translate([-203.2, -6.35, -29.36875]);

  _cache.motorMount_bounds = cube({
    size: [117.22999999999996, 63.5, 66.675]
  }).translate([-204.97999999999996, -44.44999999999999, -42.068749999999994]);

  _cache.driveMount_bounds = cube({
    size: [73.02499999999998, 38.10000000000002, 44.45000000000001]
  }).translate([-115.90499999999997, -19.05000000000001, -29.368750000000013]);
  // var screw = Parts.Cylinder(util.inch(0.368), util.inch(8))
  //   .rotateY(90)
  //   .Center("xyz")
  //   .color("blue");

  // var carriage = Carriage(screw);
  // var bracket = Bracket(screw, carriage.parts.body);
  // var motorMount = MotorMount(screw, carriage.parts.body);

  var parts = {
    hardware: () => {
      console.log("motorMount", cache("motorMount"));
      return [
        ...(params.carriage || params.carriageA || params.carriageB
          ? cache("carriage").array(
              "nut,nut1,nut2,bolt1,bolt2,washer1,washer2,boltA,washerA1,washerA2,boltB,washerB1,extensionWasher1washer,extensionWasher2washer,arm"
            )
          : []),
        ...(params.pivotMount
          ? cache("pivotMount").array(
              "collar,bearing,thrustBearing,washerA,washerB,screw1,screw2,pivotBolt,washerC,screwC,washerD,screwD"
            )
          : []),
        ...(params.motorMount
          ? cache("motorMount").array(
              "motor,motorScrew1,motorScrew2,motorScrew3,motorScrew4"
            )
          : []),
        ...(params.driveMount
          ? cache("driveMount").array(
              "collar,bearing,thrustBearing,washerA,washerB,screw1,screw2,washerC,screwC,washerD,screwD"
            )
          : [])
      ];
    },
    screw: () => cache("screw"),
    rail: () => cache("rail"),
    pivotMount: () =>
      cache("pivotMount")
        .combine()
        .subtract(cache("rail").enlarge(0.75, 0.75, 0.75)),
    driveMount: () =>
      cache("driveMount")
        .combine()
        .subtract(cache("rail").enlarge(0.75, 0.75, 0.75)),
    motorMount: () =>
      cache("motorMount").parts.motorBlock.subtract(
        cache("rail").enlarge(0.75, 0.75, 0.75)
      ),
    driveBushing: () => cache("motorMount").parts.cone,
    bracketAssembly: () => cache("pivotMount").array(),
    carriage: () => cache("carriage").array(),
    carriageA: () =>
      cache("carriage").array("bodypositive", p => p.color("gray")),
    carriageB: () => cache("carriage").array("bodynegative"),
    arms: arm,
    roller: Roller,
    bushing: () => BearingBushing(cache("screw")).array(),
    motorAdapter: MotorAdapter,
    tensionTool: () => TensionTool(cache("screw"))
  };

  var selectedParts = Object.entries(parts)
    .filter(([key, value]) => {
      return params[key];
    })
    .reduce((parts, [key, value]) => {
      var part = value();
      if (Array.isArray(part)) parts = parts.concat(part);
      else parts.push(part);
      return parts;
    }, []);

  console.log("selectedParts", selectedParts);
  var parts = selectedParts.length ? selectedParts : [util.unitAxis(20)];

  if (params.center) parts = [union(parts).Center()];
  if (params.cutawayEnable)
    parts = [util.bisect(union(parts), params.cutawayAxis).parts.positive];

  console.log("timer", performance.now() - start);
  console.log(
    "BOM\n",
    Object.entries(Hardware.BOM)
      .map(([key, value]) => `${key}: ${value}`)
      .join("\n")
  );
  return [...parts];
}

/**
 * Part functions
 */

function TensionTool(screw) {
  return Parts.Cylinder(util.inch(0.75), util.inch(2))
    .rotateY(90)
    .align(screw, "xyz")
    .subtract(screw.enlarge(Hardware.Clearances.loose))
    .bisect("z")
    .rotate("", "y", 90)
    .combine("", {}, (part, key, index) => {
      console.log("index", key, index);
      return part.translate([2 * (index % 2 == 0 ? -1 : 1), 0, 0]);
    });
}

function MotorAdapter() {
  var nut = Hardware.Nut(ImperialNuts["3/8 hex"]).color("gray");

  var shellWidth = 24;

  var shell = Parts.Cylinder(shellWidth, 10).color("orange");
  var cone = Parts.Cone(shellWidth, 8, 5)
    .snap(shell, "z", "outside-")
    .color("green");

  var post = Parts.Cylinder(8, 10)
    .snap(cone, "z", "outside-")
    .bisect("x", 1)
    .parts.positive.color("blue");

  var fit = Hardware.Clearances.close;
  return [
    post,
    shell
      .subtract(nut.enlarge(fit, fit, 0))
      .union(cone)
      .bisect("x", 1).parts.positive
  ];
}

function Roller() {
  var body = Parts.Cylinder(util.inch(2), util.inch(1));
  var lip = Parts.Cylinder(util.inch(1), 3).snap(body, "z", "outside-");
  var washer = Hardware.Washer(ImperialWashers["1/4"], "loose").snap(
    "washer",
    lip,
    "z",
    "inside+"
  );
  var washer2 = washer.clone().snap("washer", body, "z", "inside-");
  return body
    .union([lip])
    .subtract([
      washer.combine(),
      washer2.combine(),
      Hardware.Bolt(util.inch(2.5), ImperialBolts["1/4 hex"], "loose").snap(
        "head",
        util.unitCube().Zero(),
        "z",
        "outside+",
        -1
      ).parts.tap
    ])
    .color("silver");
}

function Rail(screw) {
  return (
    Parts.Cube([util.inch(16), util.inch(0.5), util.inch(0.0625)])
      .align(screw, "xyz")
      .translate([0, 0, -util.inch(1.125)])
      // .snap(util.unitCube(), "z", "inside-")
      .color("gray")
  );
}

// var BoltLengths = [1.5, 0.75, 2.5, 2, 1.75, 1, 0.5, 2.3, 3.5, 3].sort();

// ********************************************************
// Other jscad libraries are injected here.  Do not remove.
// Install jscad libraries using NPM
// ********************************************************
// include:js
// endinject
