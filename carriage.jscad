/**
 * Carriage assembly
 * @param screw
 * @param rail
 */
function Carriage(screw, rail) {
  // var nutType = ImperialNuts["3/8 hex"];
  var nutType = {
    face: 22.2,
    corner: 25.36,
    height: 12.22,
    diameter: 11.1,
    name: "1/2 acme hex"
  };
  var nut = Hardware.Nut(nutType)
    .rotateY(90)
    .align(screw, "xyz")
    .color("gray");

  var carriage = util.group({});
  carriage.add(nut, "nut", true);

  var bolt1 = Hardware.Bolt(util.inch(1.0), ImperialBolts["1/4 hex"], "loose")
    .rotate("head", "x", -90)
    .align("head", nut, "xyz")
    .snap("thread", nut, "y", "outside+");

  carriage.add(bolt1, "bolt1", true, "bolt1", "head,thread");

  var bolt2 = bolt1.clone().rotate(nut, "x", 180);

  carriage.add(bolt2, "bolt2", true, "bolt2", "head,thread");

  var locknutType = ImperialNuts["1/4 hex"];
  locknutType[2] = 8.5;
  locknutType[4] = "1/4 hex lock";
  var nut1 = Hardware.Nut(locknutType)
    .rotateX(90)
    .align(nut, "xyz")
    .snap(nut, "y", "outside+")
    .color("gray");

  carriage.add(nut1, "nut1", true);

  var nut2 = nut1.mirroredY();
  carriage.add(nut2, "nut2", true);

  // var bearing = util.group("bearing");
  // var bearing1 = Hardware.Bearing(RSeriesBearings["R4"])
  //   .rotate("bearing", "x", 90)
  //   .align("bearing", screw, "xy")
  //   .snap("bearing", rail, "z", "outside-");
  // bearing.add(bearing1, "bearing1", false, "bearing1");
  // bearing.add(
  //   bearing1.snap("bearing", bearing1.parts.bearing, "y", "outside-"),
  //   "bearing2",
  //   false,
  //   "bearing2"
  // );

  // carriage.add(
  //   bearing.align("bearing1,bearing2", rail, "y"),
  //   "bearing",
  //   true,
  //   "bearing"
  // );

  // var bearingBolt = Hardware.Bolt(
  //   util.inch(2.5),
  //   ImperialBolts["1/4 hex"],
  //   "loose"
  // )
  //   .setName("bearingBolt")
  //   .rotate("head", "x", 90)
  //   .align("thread", carriage.parts.bearing, "xyz");

  // carriage.add(bearingBolt, "bearingBolt", true, "bearingBolt", "head,thread");

  /**
   * Carriage box
   */
  var box = Parts.RoundedCube(
    util.inch(2),
    util.inch(1.0),
    util.inch(2),
    util.inch(0.225)
  )
    .rotateX(90)
    .rotateY(90)
    .align(nut, "xyz");
  // .snap(rail, "z", "outside-", util.inch(0.0625));

  /**
   * Inline washers
   */
  // var washerType = ImperialWashers["3/8"];
  var washerType = ImperialWashers["1/2"];
  var washer1 = Hardware.Washer(washerType, "loose")
    .rotate("washer", "y", 90)
    .align("washer", screw, "xyz")
    .snap("washer", box, "x", "inside-", -0.25);
  carriage.add(washer1, "washer1", true, "washer1", "washer");

  var washer2 = washer1.clone().rotate(nut, "y", 180);
  carriage.add(washer2, "washer2", true, "washer2", "washer");

  /**
   * Connection bolts
   */
  var boltA = Hardware.Bolt(util.inch(1.5), ImperialBolts["1/4 hex"], "loose")
    .rotate("head", "y", 90)
    // .align("head", washer1.parts.washer, "yz")
    .snap("thread", washer1.parts.clearance, "y", "outside+")
    .rotate(screw, "x", 45);

  var washerA1 = Hardware.Washer(ImperialWashers["1/4"], "loose")
    .rotate("washer", "y", 90)
    .snap("washer", box, "x", "outside+")
    .align("washer", boltA.parts.thread, "yz");
  carriage.add(washerA1, "washerA1", true, "washerA1", "washer");
  carriage.add(
    boltA.snap("head", washerA1.parts.washer, "x", "outside+"),
    "boltA",
    true,
    "boltA",
    "head,thread"
  );
  carriage.add(
    washerA1.clone().snap("washer", box, "x", "outside-"),
    "washerA2",
    true,
    "washerA2",
    "washer"
  );
  var boltB = boltA.clone().rotate(screw, "x", 180);
  carriage.add(boltB, "boltB", true, "boltB", "head,thread");
  carriage.add(
    washerA1.clone().align("washer", boltB.parts.thread, "yz"),
    "washerB1",
    true,
    "washerB1",
    "washer"
  );

  /**
   * Extension wing to account for washers
   */
  var extension = Parts.RoundedCube(
    util.inch(1),
    util.inch(1),
    1.45 * 2, // two 1/4" washers
    util.inch(0.225)
  )
    .rotateX(90)
    .rotateY(90)
    .snap(box, "y", "outside+")
    .align(bolt1.parts.thread, "xz");

  var extensionWasher = Hardware.Washer(ImperialWashers["1/4"], "loose")
    .rotate("washer", "x", 90)
    .rotate("washer", "y", 90)
    .snap("washer", extension, "y", "inside-")
    .align("washer", bolt1.parts.thread, "xz");
  carriage.add(
    extensionWasher,
    "extensionWasher1",
    true,
    "extensionWasher1",
    "washer"
  );
  carriage.add(
    extensionWasher.clone(p => p.mirroredY()),
    "extensionWasher2",
    true,
    "extensionWasher2",
    "washer"
  );

  var arm = Parts.Cylinder(util.inch(1), util.inch(0.25))
    .rotateX(90)
    .align(bolt1.parts.thread, "xz")
    .snap(extension, "y", "outside+")
    .color("silver");

  carriage.add(arm, "arm", true);
  console.log("carriage", carriage);
  var body = box
    .union([
      extension.subtract(carriage.parts.extensionWasher1clearance),
      extension.mirroredY().subtract(carriage.parts.extensionWasher2clearance)
    ])
    .subtract([
      bolt1.parts.tap,
      bolt2.parts.tap,
      nut1.enlarge(0.5, 5.5, 0.5).snap(nut1, "y", "inside-"),
      nut2.enlarge(0.5, 5.5, 0.5).snap(nut2, "y", "inside+"),
      nut.enlarge(0.5, 0.5, 0.5),
      screw.enlarge(1, 1, 1),
      // screw
      //   .stretch("z", 30)
      //   .snap(screw, "z", "inside+")
      //   .enlarge(1, 1, 1),
      washer1.parts.clearance,
      washer2.parts.clearance,
      boltA.parts.tap,
      boltB.parts.tap
      // bearingBolt.parts.tap,
      // carriage
      //   .combine("bearingbearing1shell,bearingbearing2shell")
      //   .enlarge([2, 2, 2])
    ])
    .bisect("x");

  body.parts.positive = body.parts.positive.color("blue", 0.5);
  body.parts.negative = body.parts.negative.color("red", 0.5);

  carriage.add(body, "body", false, "body");

  return carriage;
}
