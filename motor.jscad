/**
 * Returns a motor group object
 * @Returns group
 */
function Motor(screwSize) {
  var dshaft = Parts.Cylinder(5.1, 12);

  var shaft = Parts.Cylinder(5.1, 2.65)
    .align(dshaft, "xy")
    .snap(dshaft, "z", "outside-")
    .color("gray");

  var pto = Parts.Cylinder(12.05, 5.3)
    .align(shaft, "xy")
    .snap(shaft, "z", "outside-")
    // .snap(shaft, 'x', 'inside-', 5)
    .color("yellow");

  var body = Parts.Cylinder(37.05, 72.3)
    .snap(pto, "x", "inside-", -5)
    .snap(pto, "z", "outside-")
    .color("gray");

  var hole1 = Parts.Cylinder(4, 20)
    .align(body, "x")
    .snap(body, "z", "outside+")
    .snap(body, "y", "inside-", 1)
    .color("red");

  var screw = Hardware.Screw.FlatHead(
    ImperialWoodScrews["#8"],
    screwSize || util.inch(3 / 8),
    "loose",
    { name: "screw1" }
  )
    .align("thread", body, "x")
    .snap("head", pto, "z", "inside-")
    .snap("thread", body, "y", "inside-", 1);

  var group = util.group("motor");
  group.add(body, "body");
  group.add(pto, "pto");
  group.add(shaft, "shaft");
  group.add(dshaft.bisect("x", 1).parts.positive.color("silver"), "dshaft");
  group.holes = [
    union([
      hole1,
      util.rotateAround(hole1, body, "z", 180),
      util.rotateAround(hole1, body, "z", 60),
      util.rotateAround(hole1, body, "z", -60),
      util.rotateAround(hole1, body, "z", 120),
      util.rotateAround(hole1, body, "z", -120)
    ])
  ];

  group.add(screw, "screw1", true, "screw1", "head,thread");
  group.add(
    screw.clone("screw2").rotate(body, "z", 180),
    "screw2",
    true,
    "screw2",
    "head,thread"
  );
  group.add(
    screw.clone("screw3").rotate(body, "z", 60),
    "screw3",
    true,
    "screw3",
    "head,thread"
  );
  group.add(
    screw.clone("screw4").rotate(body, "z", -60),
    "screw4",
    true,
    "screw4",
    "head,thread"
  );

  return group;
}

function Motor2(screwSize) {
  var dshaft = Parts.Cylinder(6.1, 14);

  var shaft = Parts.Cylinder(5.1, 1)
    .align(dshaft, "xy")
    .snap(dshaft, "z", "outside-")
    .color("gray");

  var pto = Parts.Cylinder(12.05, 4.58)
    .align(shaft, "xy")
    .snap(shaft, "z", "outside-")
    // .snap(shaft, 'x', 'inside-', 5)
    .color("yellow");

  var body = Parts.Cylinder(37.1, 58.4)
    .snap(pto, "x", "inside-", -5)
    .snap(pto, "z", "outside-")
    .color("gray");

  var hole1 = Parts.Cylinder(4, 20)
    .align(body, "x")
    .snap(body, "z", "outside+")
    .snap(body, "y", "inside-", 1)
    .color("red");

  var screw = Hardware.Screw.FlatHead(
    MetricFlatHeadScrews["M3"],
    screwSize || util.inch(3 / 8),
    "loose",
    { name: "screw1" }
  )
    .align("thread", body, "x")
    .snap("head", pto, "z", "inside-")
    .snap("thread", body, "y", "inside-", 1);

  var group = util.group("motor");
  group.add(body, "body");
  group.add(pto, "pto");
  group.add(shaft, "shaft");
  group.add(dshaft.bisect("x", 1).parts.positive.color("silver"), "dshaft");
  group.holes = [
    union([
      hole1,
      util.rotateAround(hole1, body, "z", 180),
      util.rotateAround(hole1, body, "z", 60),
      util.rotateAround(hole1, body, "z", -60),
      util.rotateAround(hole1, body, "z", 120),
      util.rotateAround(hole1, body, "z", -120)
    ])
  ];

  group.add(screw, "screw1", true, "screw1", "head,thread");
  group.add(
    screw.clone("screw2").rotate(body, "z", 180),
    "screw2",
    true,
    "screw2",
    "head,thread"
  );
  group.add(
    screw.clone("screw3").rotate(body, "z", 60),
    "screw3",
    true,
    "screw3",
    "head,thread"
  );
  group.add(
    screw.clone("screw4").rotate(body, "z", -60),
    "screw4",
    true,
    "screw4",
    "head,thread"
  );

  return group;
}
