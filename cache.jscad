var _cache = {};

function bboxJsCad(part, name) {
  var bounds = part.combine ? part.combine().getBounds() : part.getBounds();
  var size = util.size(bounds);
  var cachedBounds = _cache[`${name}_bounds`];
  var cachedSize = util.size(cachedBounds);
  var jscadstr = `_cache.${name}_bounds = cube({size: [${size.x}, ${size.y}, ${size.z}]}).translate([${bounds[0].x}, ${bounds[0].y}, ${bounds[0].z}])`;
  var cachedBoundsBounds = cachedBounds.getBounds();

  if (
    cachedBounds &&
    // JSON.stringify(cachedBounds.getBounds()) != JSON.stringify(bounds) &&
    size.x != cachedSize.x &&
    size.y != cachedSize.y &&
    size.z != cachedSize.z &&
    bounds[0].x != cachedBoundsBounds[0].x &&
    bounds[0].y != cachedBoundsBounds[0].y &&
    bounds[0].z != cachedBoundsBounds[0].z
  ) {
    var message = `${name}_bounds does not match!  Replace cached version with: ${jscadstr}
${JSON.stringify(size)} ${JSON.stringify(util.size(cachedBounds))}`;
    console.error(message);
    var err = new Error(message);
    err.name = "local_cache_mismatch";
    // throw err;
  } else if (!cachedBounds) {
    console.warn(
      `${name}_bounds is not cached!  Add cached version with: `,
      jscadstr
    );
  }
}

function cache(name, bounds) {
  // console.log("Cache", name, bounds, _cache);

  if (_cache[name]) return _cache[name];
  /**
   * If you only need bounds, return the cached bounds.
   */
  if (bounds && _cache[`${name}_bounds`]) return _cache[`${name}_bounds`];

  switch (name) {
    case "screw":
      _cache[name] = Parts.Cylinder(
        util.inch(0.5) /* util.inch(0.368)*/,
        util.inch(10)
      )
        .rotateY(90)
        .Center("xyz")
        .color("blue");
      break;
    case "carriage":
      _cache[name] = Carriage(cache("screw"), cache("rail"));
      break;
    case "pivotMount":
      _cache[name] = PivotMount(cache("screw"), cache("rail"));
      break;
    case "motorMount":
      console.log("driveMount", cache("driveMount"));
      _cache[name] = MotorMount(
        cache("screw"),
        cache("rail"),
        cache("driveMount").parts.block
      );
      break;
    case "driveMount":
      _cache[name] = DriveMount(cache("screw"), cache("rail"));
      break;
    case "rail":
      _cache[name] = Rail(cache("screw_bounds"));
      break;
  }
  console.log("Cache Miss:", name, bounds, _cache);

  bboxJsCad(_cache[name], name);
  return _cache[name];
}
