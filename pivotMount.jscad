function BetterTriangle(base, height, thickness) {
  var tri = CAG.fromPoints([
    [base, 0],
    [0, height],
    [base, height]
  ]);

  return tri.extrude({
    offset: [0, 0, thickness]
  });
}

function PivotMount(screw, rail) {
  // topObject = topObject || util.unitCube(25);
  var bracket = util.group("PivotMount");

  bracket.add(
    thrustAssembly(screw).snap(
      "bearing",
      screw,
      "x",
      "inside+",
      -util.inch(1.25)
    ),
    undefined,
    true
  );

  var nutSize = bracket.combine("collar,bearing").size().x;

  var block = Parts.Cube([util.inch(0.875), util.inch(1.5), util.inch(1.75)])
    .align(screw, "y")
    .snap(bracket.parts.bearing, "x", "inside-", util.inch(0))
    .snap(rail, "z", "inside-")
    .color("green", 0.25);

  /**
   * add braces
   */

  var braceInside = BetterTriangle(
    util.inch(1.0),
    util.inch(1.75),
    util.inch(1.5)
  )
    .rotateX(-90)
    // .rotateY(-45)
    .align(block, "yz")
    .snap(block, "x", "outside+")
    .color("green", 0.25);

  var braceOutside = braceInside.mirroredX().snap(block, "x", "outside-");

  //   bracket.add(braceInsideShape, "braceInside", true);

  var washerA = Hardware.Washer(ImperialWashers["1/4"], "loose")
    .snap("washer", block, "z", "outside-")
    .snap("washer", block, "y", "inside+", 1)
    .snap("washer", block, "x", "inside+");
  bracket.add(washerA, "washerA", true, "washerA", "washer");

  var washerB = washerA.clone().snap("washer", block, "y", "inside-", -1);
  bracket.add(washerB, "washerB", true, "washerB", "washer");

  var screw1 = Hardware.Bolt(util.inch(2.5), ImperialBolts["1/4 hex"], "loose")
    .rotate("head", "y", 180)
    .snap("head", washerA.parts.washer, "z", "outside-")
    .align("head", washerA.parts.washer, "xy");
  bracket.add(screw1, "screw1", true, "screw1", "head,thread");

  var screw2 = screw1.clone().align("head", washerB.parts.washer, "xy");
  bracket.add(screw2, "screw2", true, "screw2", "head,thread");

  /**
   * outboard bolts
   */
  var washerC = Hardware.Washer(ImperialWashers["1/4"], "loose")
    .snap("washer", block, "z", "inside-")
    .snap("clearance", block, "y", "inside-", -1)
    .snap("washer", block, "x", "outside-", 1)
    .translate([0, 0, util.inch(0.75)]);
  bracket.add(washerC, "washerC", true, "washerC", "washer");

  var screwC = Hardware.Bolt(util.inch(1.25), ImperialBolts["1/4 hex"], "loose")
    .rotate("head", "y", 180)
    .snap("head", washerC.parts.washer, "z", "outside-")
    .align("head", washerC.parts.washer, "xy");
  // .translate([0, -1, 0]);
  bracket.add(screwC, "screwC", true, "screwC", "head,thread");

  var washerD = washerC
    .clone()
    .snap("washer", block, "x", "outside+", -util.inch(0.375))
    .snap("clearance", block, "y", "inside+", 1)
    .translate([0, 0, -util.inch(0.5)]);
  bracket.add(washerD, "washerD", true, "washerD", "washer");

  var screwD = screwC
    .clone()
    .align("head", washerD.parts.washer, "xy")
    .snap("head", washerD.parts.washer, "z", "outside-");
  // .translate([0, 1, 0]);
  bracket.add(screwD, "screwD", true, "screwD", "head,thread");

  var pivotBolt = Hardware.Bolt(
    util.inch(2.5),
    ImperialBolts["1/4 hex"],
    "loose"
  )
    .rotate("head", "x", -90)
    // .align("head", screw, "z")
    .align("thread", rail, "z", util.inch(0.5))
    .snap("thread", block, "x", "outside-", -util.inch(0.125))
    .snap("head", block, "y", "outside+", -util.inch(0.375));
  bracket.add(pivotBolt, "pivotBolt", true, "pivotBolt", "head,thread");
  console.log("pivotMount", bracket);

  var base = block.union([braceInside, braceOutside]);
  var baseBBox = Parts.BBox(base);
  bracket.add(
    base
      .union([
        washerC.parts.clearance
          .stretch("z", util.inch(0.5))
          .snap(washerC.parts.washer, "z", "outside+")
          .color("green")
          .intersect(baseBBox),
        washerD.parts.clearance
          .stretch("z", 4.75)
          .snap(washerD.parts.washer, "z", "outside+")
          .color("green")
          .intersect(baseBBox)
      ])
      .subtract([
        // bracket.parts.thrustBearingbottom
        //   .stretch("x", 50)
        //   .snap(bracket.parts.thrustBearingbottom, "x", "inside+"),
        bracket.parts.bearing
          .enlarge([
            0,
            Hardware.Clearances.loose * 1.5,
            Hardware.Clearances.loose * 1.5
          ])
          .stretch("x", 50)
          .snap(bracket.parts.bearing, "x", "inside+")
          .color("darkred"),
        bracket.parts.collarshell
          .stretch("x", 10)
          .snap(bracket.parts.collarshell, "x", "inside+"),
        screw.enlarge(1, 1, 1),
        screw1.parts.tap,
        screw2.parts.tap,
        pivotBolt.parts.tap,
        washerC.parts.clearance.stretch("z", 50).color("darkred"),
        screwC.parts.tap,
        washerD.parts.clearance.stretch("z", 50).color("darkred"),
        screwD.parts.tap
      ]),
    "block"
  );

  return bracket;
}
