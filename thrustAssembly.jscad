function thrustAssembly(screw) {
  var g = util.group("thrustAssembly");
  // var bushing = Bushing(screw);
  // g.add(bushing, "bushing", false, "bushing", "base,interior");
  var bearing = Bearing_1_2(screw);
  g.add(bearing, "bearing", false);

  var thrustBearing = ThrustBearing_1_2_pin_rollers()
    .rotate("middle", "y", -90)
    .align("middle", screw, "xyz")
    .snap("bottom", bearing, "x", "outside+");
  g.add(
    thrustBearing,
    "thrustBearing",
    false,
    "thrustBearing",
    "top,middle,bottom"
  );

  var collar = Collar(screw, thrustBearing.parts.top);
  g.add(collar, "collar", false, "collar", "collar");

  // var cone = Parts.Cone(
  //   ImperialWashers["3/8"].od - 0.5,
  //   20,
  //   ImperialNuts["3/8 lock"][2]
  // )
  //   .rotateY(90)
  //   .snap(lockNut.parts.washer1, "x", "outside-")
  //   .subtract(lockNut.parts.nut1shell.enlarge(0.5, 0.5, 0.5))
  //   .color("orange", 0.5);
  // g.add(cone, "cone");
  console.log("ThrustAssembly", g);
  return g;
}

function Collar(screw, alignpart) {
  var g = util.group("collar");
  var collar = Parts.Cylinder(28.38, 10.3)
    .rotateY(90)
    .align(screw, "yz")
    .snap(alignpart, "x", "outside+")
    .subtract(screw)
    .color("black");
  g.add(collar, "collar");

  /**
   * Add a hidden shell for use when subtracting a place for the bearing
   */
  g.add(
    Parts.Cylinder(36, 10.3)
      .rotateY(90)
      .align(collar, "xyz")
      .color("green"),
    "shell",
    true
  );
  return g;
}

function Bushing(screw) {
  var thrustBearingThickness = 8.88;
  var depth = (thrustBearingThickness / 3) * 2;
  var baseThickness = util.nearest.over(2.5, 0.2);
  var g = util.group("bushing");
  var bore = Parts.Cylinder(10, util.inch(12))
    .rotateY(90)
    .align(screw, "xyz")
    .enlarge([
      Hardware.Clearances.loose * 1.5,
      Hardware.Clearances.loose * 1.5,
      Hardware.Clearances.loose * 1.5
    ]);

  g.add(
    Parts.Cylinder(28, baseThickness)
      .rotateY(90)
      .align(screw, "yz")
      .snap(screw, "x", "inside+")
      .subtract(bore)
      .color("yellow"),
    "base"
  );

  // g.add(bore, "bore");

  g.add(
    Parts.Cylinder(15 - Hardware.Clearances.close, depth)
      .rotateY(90)
      .align(screw, "yz")
      .snap(screw, "x", "inside+", -baseThickness)
      .subtract(bore)
      .color("yellow"),
    "interior"
  );

  return g;
}

function nutWasherNut(screw) {
  var g = util.group("nutWasherNut");
  var nut1 = Hardware.Nut(ImperialNuts["3/8 lock"])
    .rotateY(90)
    .align(screw, "yz")
    .snap(screw, "x", "inside+", -4)
    .color("gray");
  g.add(nut1.subtract(screw), "nut1");
  g.add(nut1, "nut1shell", true);

  // var washer1 = Hardware.Washer(ImperialWashers["3/8"], "loose")
  //   .rotate("washer", "y", 90)
  //   .align("washer", screw, "xyz")
  //   .snap("washer", nut1, "x", "outside+");
  // g.add(washer1, "washer1", false, "washer1", "washer");

  var nut2 = Hardware.Nut(ImperialNuts["3/8 lock"])
    .rotateY(90)
    .align(screw, "yz")
    .snap(nut1, "x", "outside+")
    .color("gray");
  g.add(nut2.subtract(screw), "nut2");
  g.add(nut2, "nut2shell", true);

  return g;
}

function Bearing_1_2(screw) {
  return Parts.Cylinder(28.58, 7.92)
    .rotateY(90)
    .align(screw, "yz")
    .color("gray")
    .subtract(screw);
}

function BearingBushing(screw) {
  var thrustBearingThickness = 8.88;
  var depth = (thrustBearingThickness / 3) * 2;
  var baseThickness = util.nearest.over(2.0, 0.2);
  var g = util.group("bushing");
  var bore = Parts.Cylinder(17, util.inch(12))
    .rotateY(90)
    .align(screw, "xyz")
    .enlarge([
      Hardware.Clearances.loose * 1.5,
      Hardware.Clearances.loose * 1.5,
      Hardware.Clearances.loose * 1.5
    ]);

  g.add(
    Parts.Cylinder(28.58, baseThickness)
      .rotateY(90)
      .align(screw, "yz")
      .snap(screw, "x", "inside+")
      .subtract(bore)
      .color("yellow"),
    "base"
  );

  // g.add(bore, "bore");

  // g.add(
  //   Parts.Cylinder(15 - Hardware.Clearances.close, depth)
  //     .rotateY(90)
  //     .align(screw, "yz")
  //     .snap(screw, "x", "inside+", -baseThickness)
  //     .subtract(bore)
  //     .color("yellow"),
  //   "interior"
  // );

  return g;
}
